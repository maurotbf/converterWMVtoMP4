# Conversor de Archivos WMV a MP4

Este script de Python convierte archivos WMV a formato MP4 utilizando la herramienta `ffmpeg`. Es útil cuando necesitas convertir varios archivos de video a un formato específico de manera automatizada.

## Instalación

Asegúrate de tener instalado `ffmpeg` en tu sistema. Puedes encontrar instrucciones de instalación en su [sitio web oficial](https://ffmpeg.org/download.html).

```bash
# Instalación de ffmpeg en Ubuntu
sudo apt install ffmpeg

Uso
Descarga el script convertidor.py.
Ejecuta el script proporcionando el directorio de entrada y el directorio de salida como argumentos.

python convertidor.py <directorio_entrada> <directorio_salida>

Estructura de Directorios

convertidor.py: El script principal para la conversión de archivos.
README.md: Este archivo que proporciona información sobre el proyecto.
Dependencias
Python 3.x
ffmpeg
Contribución
Si deseas contribuir al proyecto, ¡siéntete libre de enviar una solicitud de extracción!

.

Contacto
Para preguntas o comentarios, puedes contactar al autor del proyecto a través de correo electrónico.
maurobonadeoit@gmail.com