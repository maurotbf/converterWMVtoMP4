import os
import subprocess
from glob import glob
import threading

def convertir_a_mp4(archivo_entrada, archivo_salida):
    comando = f'ffmpeg -i "{archivo_entrada}" "{archivo_salida}"'
    resultado = subprocess.run(comando, shell=True, capture_output=True, text=True)
    
    if resultado.returncode != 0:
        print(f'Error al convertir {archivo_entrada} a {archivo_salida}:')
        print(resultado.stderr)
    else:
        print(f'Convertido: {archivo_entrada} -> {archivo_salida}')

def convertir_todos_a_mp4(directorio_entrada, directorio_salida):
    # Directorios
    os.makedirs(directorio_salida, exist_ok=True)

    # Buscar todos los archivos WMV en el directorio de entrada
    archivos_wmv = glob(os.path.join(directorio_entrada, '*.wmv'))
    print(f'Archivos WMV encontrados: {archivos_wmv}')
    print(f'Ruta de entrada: {directorio_entrada}')
    print(f'Ruta de salida: {directorio_salida}')

    # Lista para almacenar los hilos
    hilos = []

    for archivo_wmv in archivos_wmv:
        # Crear el nombre del archivo de salida cambiando la extensión a .mp4
        nombre_base = os.path.splitext(os.path.basename(archivo_wmv))[0]
        archivo_mp4 = os.path.join(directorio_salida, f'{nombre_base}.mp4')

        print(f'Iniciando conversión: {archivo_wmv} -> {archivo_mp4}')

        # Convertir el archivo WMV a MP4
        convertir_a_mp4(archivo_wmv, archivo_mp4)
        print(f'Convertido: {archivo_wmv} -> {archivo_mp4}')

        # Crear el hilo para convertir cada archivo
        hilo = threading.Thread(target=convertir_a_mp4, args=(archivo_wmv, archivo_mp4))
        hilos.append(hilo)
        hilo.start()
        print(f'Creado hilo para convertir: {archivo_wmv}')

    # Esperar a que todos los hilos terminen
    for hilo in hilos:
        hilo.join()
        print(f'Hilo terminado')

    print('Proceso de conversión completado.')

# Ejemplo de uso:
directorio_entrada = "."  # Directorio actual
directorio_salida = "./output"  # Subdirectorio llamado "output" en el directorio actual

convertir_todos_a_mp4(directorio_entrada, directorio_salida)
 